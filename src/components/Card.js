// @flow

import React from 'react';
import { TouchableWithoutFeedback, Animated, Image, StyleSheet } from 'react-native';
import _LinearGradient from 'react-native-linear-gradient';

import cardImage from '../images/card.png';

const LinearGradient = Animated.createAnimatedComponent(_LinearGradient);

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
  },
  image: {
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 10,
    resizeMode: 'contain',
    flex: 1,
  },
  highlight: {
    borderRadius: 12,
  },
  gradient: {
    flex: 1,
    borderRadius: 12,
  }
})


type Props = {
  onPress?: Function,
  onLongPress?: Function,
  style: ?Object,
  width: number,
  height: number,
  gradientStyle: Object,
}


export default class Card extends React.Component {
  props: Props


  render() {
    const {
      width,
      height,
      onPress,
      onLongPress,
      style,
      gradientStyle,
    } = this.props;

    const commonStyle = { width, height };

    return (
      <Animated.View
        style={[styles.container, commonStyle, style]}
        renderToHardwareTextureAndroid
        shouldRasterizeIOS
      >
        <TouchableWithoutFeedback
          onPress={onPress}
          onLongPress={onLongPress}
          style={styles.highlight}
        >
          <Image
            style={[styles.image, commonStyle]}
            source={cardImage}
          >
            <LinearGradient
              style={[styles.gradient, gradientStyle]}
              colors={['transparent', 'rgba(0, 0, 0, 0.8)']}
            />
          </Image>
        </TouchableWithoutFeedback>
      </Animated.View>
    );
  }
}
