// @flow

import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
} from 'react-native';

import CardsList from './CardsList';

const styles = StyleSheet.create({
  app: {
    height: Dimensions.get('window').height,
  }
});

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.app}>
        <CardsList />
      </View>
    );
  }
}