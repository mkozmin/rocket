// @flow

import React from 'react';
import RN, {
  StyleSheet,
  Dimensions,
  Animated,
  PanResponder,
} from 'react-native';


import Card from '../components/Card';
import createId from '../lib/createId';


const CARD_ASPECT_RATIO = 686 / 432;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});


type State = {
  cards: Array<Object>,
}

type Props = {
  minCardOffset: number,
  maxCardOffset: number,
  cardsAmount: number,
}


export default class CardsList extends React.Component {

  state: State
  props: Props
  panResponder: RN.PanResponderStatic
  scrollY: Animated.Value = new Animated.Value(0);


  static defaultProps = {
    cardsAmount: 15,
    minCardOffset: 0.2,
    maxCardOffset: 0.6
  }


  constructor(props: Props) {
    super(props);

    this.state = {
      cards: this.generateCards(),
    };

    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onMoveShouldSetPanResponder: (_, { dy }) => Math.abs(dy) > 5,
      onPanResponderGrant: () => {
        this.scrollY.stopAnimation(value => {
          this.scrollY.setOffset(value);
          this.scrollY.setValue(0);
        });
      },
      onPanResponderMove: (_, { dy }) => {
        this.scrollY.setValue(dy * 1.2);
      },
      onPanResponderRelease: () => {
        this.scrollY.flattenOffset();
        this.scrollY.stopAnimation(value => {
          const { min, max } = this.calcScrollBounds(this.props.cardsAmount);
          let to;

          if (value < min) to = min;
          if (value > max) to = max;
          if (typeof to === 'undefined') {
            return;
          }
            Animated.spring(this.scrollY, { toValue: to }).start();
        });
      }
    })
  }


  generateCards(): Array<Object> {
    const { minCardOffset, maxCardOffset, cardsAmount } = this.props;


    const cards = [];
    const cardHeight = this.calcCardDimensions().height;
    const { max: maxScroll } = this.calcScrollBounds(cardsAmount);
    const initiallyVisibleCardsAmount = this.calcInitiallyVisibleCardsAmount();

    const springConfig = {
      friction: 12,
      tension: 120,
    };

    for (let i = 0; i < cardsAmount; i++) {
      const minPossibleOffset = i < cardsAmount - initiallyVisibleCardsAmount
        ? 0
        : cardHeight * minCardOffset * (i - cardsAmount + initiallyVisibleCardsAmount + 1);
      
      const maxPossibleOffset = (cardHeight * maxCardOffset) * i;
      
      const position = new Animated.Value(minPossibleOffset);

      Animated.spring(position,
        {
          ...springConfig,
          toValue: this.scrollY.interpolate({
            inputRange: [
              Number.NEGATIVE_INFINITY,
              cardHeight * maxCardOffset * (cardsAmount - 1 - i),
              maxScroll,
              Number.POSITIVE_INFINITY
            ],
            outputRange: [
              minPossibleOffset,
              minPossibleOffset,
              maxPossibleOffset,
              maxPossibleOffset
            ],
          }),
        }
      ).start();

      cards.push({ id: createId(), position });
    }

    return cards;
  }


  calcScrollBounds(numCards: number): { min: number, max: number } {
    const { height } = this.calcCardDimensions();
    const { maxCardOffset } = this.props;
    return {
      min: 0,
      max: (numCards - 1) * (height * maxCardOffset),
    };
  }

  
  calcCardDimensions(): { width: number, height: number} {
    const width = Dimensions.get('screen').width * 0.7;
    const height = width / CARD_ASPECT_RATIO;
    return { width, height };
  }


  calcContentHeight(): number {
    const { height } = this.calcCardDimensions();
    const { length } = this.state.cards;
    return (length - 1) * height * 0.5 + height;
  }


  calcInitiallyVisibleCardsAmount(): number {
    return 4;
  }


  renderCards() {
    const { cardsAmount, minCardOffset } = this.props;
    const { cards } = this.state;
    const { width: cardWidth, height: cardHeight } = this.calcCardDimensions();
    const maxScroll = this.calcScrollBounds(cardsAmount).max;
    const { width: windowWidth } = Dimensions.get('window');

    const commonStyle = {
      position: 'absolute',
      top: 50,
      left: (windowWidth - cardWidth) / 2,
    };

    const rotateX = this.scrollY.interpolate({
      inputRange: [
        Number.NEGATIVE_INFINITY,
        -300,
        0,
        maxScroll,
        maxScroll + 300,
        Number.POSITIVE_INFINITY
      ],
      outputRange: [
        '0deg',
        '0deg',
        '-22deg',
        '-22deg',
        '-46deg',
        '-46deg'
      ]
    });

    
    const gradientStyle = {
      opacity: this.scrollY.interpolate({
        inputRange: [
          Number.NEGATIVE_INFINITY,
          -300,
          0,
          maxScroll,
          maxScroll + 300,
          Number.POSITIVE_INFINITY
        ],
        outputRange: [
          0,
          0,
          0.7,
          0.7,
          1,
          1
        ],
      })
    }

    return cards.map(({ id, position }, index) => {
      const opacity = index === 0 ? 1 : position.interpolate({
        inputRange: [Number.NEGATIVE_INFINITY, 0, cardHeight * minCardOffset],
        outputRange: [0, 0, 1],
      });
      return (
        <Card
          key={id}
          width={cardWidth}
          height={cardHeight}
          style={
            {
            ...commonStyle,
            opacity,
            zIndex: index + 1,
            transform: [
              { translateY: position },
              { rotateX }
            ]
            }
          }
          gradientStyle={gradientStyle}
        />
      )}
    );
  }


  render() {
    return (
      <Animated.View
        style={[styles.container, {
          transform: [
           { rotateX: this.scrollY.interpolate({
              inputRange: [
                Number.NEGATIVE_INFINITY,
                -900,
                -300,
                0
              ],
              outputRange: [
                '10deg',
                '10deg',
                '0deg',
                '0deg',
              ],
            })
           }
          ]
        }]}
        {...this.panResponder.panHandlers}
        renderToHardwareTextureAndroid
        shouldRasterizeIOS
      >
        {this.renderCards()}
      </Animated.View>
    );
  }
}